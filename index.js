console.log('***Test Init***');

/*
 * 1) Si tuviera una esfera (por ejemplo la tierra) y tuviera 2 puntos aleatorios
 *      a) Calcular la distancia en metros entre esos 2 puntos.
 */
console.log('1#');

var lat1 = -27.416340;
var lon1 = -55.931910;
var lat2 = -27.415355;
var lon2 = -55.931347;

function distancia(lat1, lon1, lat2, lon2) {
    
    rad = function (x) {
        return x * Math.PI / 180;
    }

    var R = 6378.137;   //r de la tierra en km
    var dLat = rad(lat2 - lat1);
    var dLong = rad(lon2 - lon1);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d.toFixed(3)*1000;

}

distancia_entre_2 = distancia(lat1, lon1, lat2, lon2);

console.log ('Distancia: ' + distancia_entre_2 + ' m');


/*
 * 2) Si tuviera una serie de puntos ordenados (lat, lng), una marca de tiempo (timestamp) y datos agregados de entorno (siguiendo este formato: {IDENTIFICADOR: VALOR}) por cada punto:
 *     a) Calcular la distancia final en metros del trayecto total. [ok]
 *     b) Calcular el tiempo total del trayecto. [ok]
 *     c) Calcular la velocidad entre cada punto, si estuviera recorriendo los puntos de forma aislada.[ok]
 *     d) Calcular la velocidad promedio de todo el trayecto.
 *     e) Agrupar los datos comunes del entorno
 */
console.log('2#');

var time = Date.now();
var now = new Date();
function date_for_human( timestamp ){
    var algo = new Date( timestamp );
    return algo.getHours() + ':' + algo.getMinutes() + ':' + algo.getSeconds();
}

var time_acum;

/*
Opté por usar time como timepo acumulado en segundos en lugar de marcas de tiempo (timestamp) a fin de simplificar los calculos. 
*/
points = [
    { 'lat':-27.416340, 'long':-55.931910, 'time': 0, 'dist': 0 },
    { 'lat':-27.415355, 'long':-55.931347, 'time': 20, 'dist': 0 },
    { 'lat':-27.414855, 'long':-55.931347, 'time': 30, 'dist': 0 },
    { 'lat':-27.413055, 'long':-55.931347, 'time': 40, 'dist': 0 },
    { 'lat':-27.412955, 'long':-55.931347, 'time': 50, 'dist': 0 },
    { 'lat':-27.411055, 'long':-55.931347, 'time': 60, 'dist': 0 },
]

let distancia_acumulada = 0;
let tiempo_total = 0;
let velocidad_promedio = 0;
let datos_agrupados;
let tiempo_acum = 0;

function calculate_params( points ){

    
    for ( i=0; i < points.length; i++ ){
        let dist = 0;
        if( i < points.length - 1 ){
            dist = distancia(points[i]['lat'], points[i]['long'], points[i+1]['lat'], points[i+1]['long']);
            console.log(dist);
            points[i+1]['dist'] = dist;
            distancia_acumulada +=dist;
        }
        points[i]['vel'] = points[i]['dist'] / points[i]['time'];
        //tiempo_acum += points[i]['time'];
    }
    velocidad_promedio = distancia_acumulada / points[ points.length -1 ]['time']
    tiempo_total = points[ points.length -1 ]['time'] - points[0]['time'];
}

calculate_params( points );

console.log('Distancia acumulada             : ' + distancia_acumulada + ' m');
console.log('Tiempo total del trayecto       : ' + tiempo_total + ' segundos');
console.log('Valocidad promedio del trayecto : ' + velocidad_promedio + ' segundos');

//console.log(points);

/*
 * 3) Si tuviera un punto central (lat, long) y múltiples puntos dispersos en una esfera.
 * 
 *   a) Calcular el punto más cercano
 *   b) Calcular el punto más lejano
 *   c) Calcular cuál sería el radio de un circulo si tuviera que aislar todos los puntos tomando como centro el punto central.
 *
 */
console.log('3#');

/* El Primer puento de la lista es el punto central y los demas multiples puentos dispersos a la redonda y a diferentes distancias  */

points = [
    { 'lat':-27.416340, 'long':-55.931910 , 'dist':0 },
    { 'lat':-27.417549, 'long':-55.930022 , 'dist':distancia(points[0]['lat'], points[0]['long'], points[1]['lat'], points[1]['long']) }, 
    { 'lat':-27.416550, 'long':-55.933648 , 'dist':distancia(points[0]['lat'], points[0]['long'], points[2]['lat'], points[2]['long']) }, 
    { 'lat':-27.413864, 'long':-55.935365 , 'dist':distancia(points[0]['lat'], points[0]['long'], points[3]['lat'], points[3]['long']) }, 
    { 'lat':-27.417549, 'long':-55.930022 , 'dist':distancia(points[0]['lat'], points[0]['long'], points[1]['lat'], points[1]['long']) }, 
    { 'lat':-27.416550, 'long':-55.933648 , 'dist':distancia(points[0]['lat'], points[0]['long'], points[2]['lat'], points[2]['long']) }, 
    { 'lat':-27.413864, 'long':-55.935365 , 'dist':distancia(points[0]['lat'], points[0]['long'], points[3]['lat'], points[3]['long']) }, 
]

//console.log( points );

points.sort(function (a, b) {
    if (a.dist > b.dist) {
      return 1;
    }
    if (a.dist < b.dist) {
      return -1;
    }
    // a must be equal to b
    return 0;
  });


console.log('a) El mas cercano: ' + points[1]['lat'] + ':' + points[1]['long'] +  ' Distancia: ' + points[1]['dist'] + ' m');
console.log('b) El mas lejano : ' + points[points.length - 1]['lat'] + ':' + points[points.length - 1]['long'] +  ' Distancia: ' + points[points.length - 1]['dist'] + ' m');
console.log('c) EL radio que tendria que tomar para formar un circulo partiendo del punto central y asi poder aislar todos los puntos seria la distancia mayor que es: ' + points[points.length - 1]['dist'] + ' m');

console.log('4#');

/*
 *  4) Si tuviera un mecanismo donde tuviera un emisor con múltiples receptores del mensaje y suponiendo que:
 *
 *   Los receptores pueden o no recibir el mensaje por desconexión, pero al volver a conectarse deben estar sincronizados.
 *   Los mensajes pueden ir a un receptor en particular.
 *   Los mensajes deben ser secretos para un receptor en particular, y en el conjunto.
 *   Los receptores pueden ser 5 o 50000 sin previo aviso.
 *
 *    a) Describir (narrativamente) ese mecanismo, tomando una o más herramientas del siguiente kit de opciones:
 *
 *        Eclipse Mosquitto (An open source MQTT broker)
 *        Google Pub/Sub
 *        WebSocket
 *        NodeJS
 *        Express
 *        Google Firebase (Realtime DB)
 *        Google Firebase (Firestore)
 *        Google CloudFunctions
 *        Docker
 *        Kubernetes
 *        Google Cloud Storage
 *
 *    b) Justificar la selección con criterios propios, como así también:
 *
 *        Escalabilidad
 *        Ductilidad
 *        Auditabilidad
 *        Costo unitario / escala
 */
/*

* MQTT (publish/subscribe messaging protocol)
 Para tal caso utilizaria el protocolo MQTT.
 
 El protocolo MQTT soporta varios niveles de QoS  (Quality of Service) los cuales describo brevemente a continuacion:
 0: El intermediario / cliente entregará el mensaje una vez, sin confirmación. 
 1: El agente / cliente entregará el mensaje al menos una vez, con la confirmación requerida.
 2: El intermediario / cliente entregará el mensaje exactamente una vez. 

 En tal caso el Qo2 seria el mas recomendado para este escenario para asegurar la llegada de los mensajes y asi
 poder garantizar la correcta sincronizacion. 

 El broker sera el encargado de gestionar las suscripciones/publicaciones de los clientes. La opcion de configurado para 
 que retenga todos los mensajes, garantizaria en caso de desconexion, que los mensajes no se perdieran. Al restablecerse la conexion nuevamente los mensajes serian entregados a destino. La actualizacion seria instantanea.

 Al trabajar con un mecanismo de suscriptores/publicadores facilmente se pordria controlar el flujo de mensajes
 permitiendo a determinados clientes conectarse a determinados topicos y asi garantizar exclusividad. 
 
 Para atender la alta demanda de clientes una opcion seria trabajar con multiples brokers ofreciendo
 asi la posibilidad de balancear la carga entre los mismos
 y limitar asi el numero de conexiones simultaneas. 

 Implementacion:
 Me surgen dos opciones:
 La primera opcion es Eclipse Mosquitto (An open source MQTT broker) 
 La segunda opcion es Mosca  un broker mqtt que corre sobre node.js. + Express
    -esta segunda opcion mas interesante por el hecho de correr sobre node js lo cual lo hace altamente flexible.  
    -Esta segunda opcion soporta hasta Qo1. 
 Cualquiera de las dos opciones serian montadas sobre tecnologia de contenedores Docker para facilitar instalacion, el manejo y manetenimiento.
 La escalabilidad estaria asegurada al estar utilizando docker + Kubernetes ya que la infrastructura seria capaz de crecer automaticamente, permitiria el balanceo de carga
 de acuerdo a la demanda.

 Toda la infraestructura seria montada en Google Cloud Platform lo cual permitiria un mayor manejo de costos a medida que se va escalando ya que 
 cuenta con un compendio de herramientas muy complero para este fin.




 */
